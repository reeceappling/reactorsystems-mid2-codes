


//Exam2Q1.js

//notes: npm library colors is used to prettyfy outputs, to install colors (DO IN THIS DIRECTORY)
//		npm install colors
const colors = require('colors');

// =================================================================================================================================================================
// ===================================================================== PROBLEM DATA ==============================================================================

const P = 2250;//psia, coolant pressure
function Tavg(relPower){return (560+(30*relPower));} //Function given to find Tavg at specific Rx power levels
const V = 13500;//ft^3, volume of primary coolant less pressurizer

// =================================================================================================================================================================
// ===================================================================== GENERIC FUNCTIONS =========================================================================

const problemNo = 1;//Problem number
var part = "a";//initial problem part to be solved

//IMPORT GENERIC FUNCTIONS
let ci = require("./Exam2functions");

const startNewPart = ci.snp, blank = ci.blk, displayInfo = ci.dI, starLine = ci.sL,solnHeader = ci.sH, colorme = ci.cme, colorOut = ci.cO, spaces = ci.sp();

//import color name variables 
var u=ci.c;
const green=u.g, cyan=u.c, yellow=u.y, magenta=u.m, white=u.w;

ci = null;u = null;//clear data from now unnecessary varibles

// =================================================================================================================================================================
// ===================================================================== PROGRAM RUNTIME ===========================================================================

displayInfo("Midterm 2, Problem "+problemNo,"Reece Appling","4/23/2021",part);//----------------------------REPLACE FINISHED DATE-------------------------------------

//Begin Part A
//Assuming no let down, how much water (lbm) must the pressurizer be able to accommodate in going from HZP to full power?

console.log("Given Pressure for coolant = ",P," psia");blank();//Display given pressure and then blank line
//Generate object with minimums and maximums for power and avg coolant temp, log info
let rx = {}; //create reactor object
rx.power = [0,1];
rx.Tavg = [Tavg(rx.power[0]),Tavg(rx.power[1])];
//Display Tavg values for min and max
console.log("From the provided Average Temperature equation...");
console.log("Tavg @   ",0,"% power: ",rx.Tavg[0],"°F");
console.log("Tavg @ ",100,"% power: ",rx.Tavg[1],"°F");blank();
//From steam tables, find densities of 0 and 100% power coolant
rx.rho = [46.1882,43.9983]; //lb/ft^3
console.log("From Steam Tables")
colorOut(["Fluid density at   ",0," % power and P = ",2250," psia: ",rx.rho[0]," lbm/ft^3"],[white,yellow,white,yellow,white,yellow,white]);
colorOut(["Fluid density at ",100," % power and P = ",2250," psia: ",rx.rho[1]," lbm/ft^3"],[white,yellow,white,yellow,white,yellow,white]);blank();
//Get coolant masses for 0% and 100% power for all but pressurizer
rx.Mcoolant = rx.rho.map(function(rho){return (V*rho);});
console.log("Coolant mass within rx volume only");
console.log("M @   ",0,"% power: ",colorme(rx.Mcoolant[0].toFixed(2),yellow)+" lbm");
console.log("M @ ",100,"% power: ",colorme(rx.Mcoolant[1].toFixed(2),yellow)+" lbm");blank();
//Find the difference between the two masses
let dMcoolant = rx.Mcoolant[0]-rx.Mcoolant[1];
colorOut(["The rx coolant volume (neglecting the pressurizer) at ",100+"%"," relative power holds"],[cyan,yellow,cyan]);
console.log(colorme(dMcoolant.toFixed(2)+" lbm","green")+colorme(" less than it did at 0%","cyan"));blank();
//print solution for part a
solnHeader(problemNo,part);
console.log(colorme(spaces+dMcoolant.toFixed(2)+" lbm","green"));
blank();

//Begin Part B
//If the pressurizer volume is 1800 ft3 and has an initial HZP liquid level of 30 %, what would be the final
//	liquid level assuming all liquid is contained within the pressurizer as saturated liquid?

part = "b";startNewPart(part);
const PRZ = {};
PRZ.V = 1800; //ft^3 as given in part b prompt
console.log("Given PRZ Volume: ",PRZ.V," ft^3");
//set liquid levels of PRZ, 2nd level temporarily set to 0
PRZ.liqLevel = [0.3,0];
console.log("Given initial PRZ liquid level: ",30,"%");blank();
//Get saturation proerties for liquid
const rhof = 37.0906; //lb/ft3
console.log("Saturated liquid density at P=",2250,"psia: ",rhof," lb/ft^3");
//get initial PRZ liquid mass
PRZ.Mliq = [PRZ.V*PRZ.liqLevel[0]*rhof,0]
//add dMcoolant to initial to find final mass
PRZ.Mliq[1] = PRZ.Mliq[0]+dMcoolant;
console.log("Mass of saturated liquid = (PRZ Volume)*(liquid level, [0,1])*(Sat Liquid Density)");
console.log("Once the initial liquid mass is found, the answer from part a can be added to find the final mass");
blank();
console.log("Initial PRZ liquid mass = "+colorme(PRZ.Mliq[0].toFixed(3),yellow)+" lbm");
console.log("Change  in  liquid mass = "+colorme(dMcoolant.toFixed(2),yellow)+" lbm, (Solution of part a)");
console.log("Final   PRZ liquid mass = "+colorme(PRZ.Mliq[1].toFixed(3),yellow)+" lbm");
blank();
console.log("Final liquid level is found by dividing its mass by the PRZ volume and the saturated liquid density");blank();
//find final liquid level from final mass and rhof
PRZ.liqLevel[1] = PRZ.Mliq[1]/(PRZ.V*rhof);
colorOut([PRZ.Mliq[1].toFixed(3)+" lbm"," of saturated liquid would take up ",(PRZ.liqLevel[1]*100).toFixed(3)+"%"," of the PRZ volume"],[yellow,cyan,green,cyan]);
blank();
//print solution for part b
solnHeader(problemNo,part);
console.log(colorme(spaces+(PRZ.liqLevel[1]*100)+" %",green));
console.log(colorme(spaces+"or, approximately:",green));
console.log(colorme(spaces+(PRZ.liqLevel[1]*100).toFixed(3)+" %",green));
blank();

//Begin Part C 
//If the final level at full power is desired to be 60%, how much coolant (lbm) must be let down during the heat up.
part = "c";startNewPart(part);
//set liquid levels of PRZ, 2nd level set to known ideal of 60%
let przLevelIdeal = 0.6;
console.log("Ideal PRZ saturated liquid level = ",60,"%");
blank();
console.log("Mass required for 60% at HFP = (PRZ Vol)*0.6*(saturated liquid density)");blank();
//find mass required for 60%;
let mIdeal = PRZ.V*przLevelIdeal*rhof;
console.log("Final mass required for ",60,"% at HFP = ",colorme(mIdeal.toFixed(3),yellow),"lbm");blank();
console.log("Final mass in PRZ "+colorme("without","magenta")+" any coolant letdown was found in "+colorme("part b (final PRZ liquid mass)","yellow"));
console.log("Final mass in PRZ "+colorme("without","magenta")+" any coolant let down = ",colorme(PRZ.Mliq[1].toFixed(3),yellow),"lbm");
blank();
//Letdown is the difference between the required mass and the mass without letdown
let letDown = PRZ.Mliq[1]-mIdeal;
colorOut(["The difference in the above two masses = ",letDown.toFixed(3)+" lbm"],[cyan,green]);
console.log(colorme("Therefore",cyan));
colorOut([letDown.toFixed(3)+" lbm"," of coolant must be let down to achieve HFP PRZ level of ",60," %"],[green,cyan,yellow,cyan]);
blank();
//print solution for part c
solnHeader(problemNo,part);
colorOut([spaces+letDown.toFixed(3)+" lbm"],[green]);
blank();