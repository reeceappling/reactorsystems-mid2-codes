



// Exam2Q2.js



const THZP = 560;//Tavg at HZP in deg F

let BOC = {};
BOC.CoreExcessReactivity = 15334;//BOC core excess reactivity with no Xe or Sm, in pcm
BOC.fixedBPWorth = -5600;//BOC Fixed burnable poison worth in pcm
BOC.Doppler = -9.8;//BOC Doppler-Only Power Coefficient, in pcm/% Power

let equilibrium = {};
equilibrium.symbols = ["Xe","Sm"];
equilibrium.worths = [-2853,-463];

solubleBoronCoeff = -7;//Soluble Boron Coefficient, in pcm/ppm

const bankSteps = 228;//total number of steps per bank
const stepOverlap = 100;//bank movement step overlap
const maxSteps = (128*3)+228;
const startNext = 128;// # steps when next bank should begin movement (steps-overlap)
const bankOrder = ["A","B","C","D"];//Bank Order for OUTWARD
const bankWorths = [555,944,1164,893];//bank worths in pcm for corresponding bank in bankOrder

//Function to show bank positions (in steps) given the total number of steps (overlaps count as 1)
function bankPositionStepsWithdrawn(step){
	let arr = [bankSteps,bankSteps,bankSteps,bankSteps];//array of fully inserted (228 steps) rod banks.
	let remainder = (step%startNext);//finds remainder of number of steps/128
	let mainActive = (step-remainder)/startNext;//finds rod bank which is less than 128 steps up in its travels
	if(mainActive==4){return [0,0,0,stepOverlap-remainder];}//if rod bank "4" (E) is active (doesnt exist in reality), last bank is only one moving
	//This next error check should never happen unless a bad step number is used.
	if(mainActive<0||mainActive>3){console.log(colorme("ERROR: bankPositionStep("+step+") found a nonexistent active bank!","red"));}
	//Active bank will always be at position 228-remainder
	arr[mainActive] = bankSteps-remainder;
	if(remainder<stepOverlap){
		//If the active bank has not yet moved 100, the previous is still moving up
		//Previous bank (if exists) will be 100-remainder steps away from full withdrawal, all previous will be at 0
		if(mainActive>0){arr[mainActive-1] = stepOverlap-remainder;for(var i=0;i<mainActive-1;i++){arr[i]=0;}}
		
	}
	else{for(var i=0;i<4;i++){if(mainActive>i){arr[i]=0;}}}//Case For when only this bank is moving, all earlier banks are at 0 steps in
	return arr;
}

//Function to give a single rod bank position to generate all other rod bank positions
function banksFromBank(bank,stepsInserted){
	arrOut = [0,0,0,0];
	arrOut[bank] = stepsInserted;
	//Go backwards if possible (work towards "A" bank)
	for(var i=0;i<bank;i++){if(arrOut[bank-i]>startNext){arrOut[bank-1-i] = arrOut[bank-i]-startNext;}}
	//Go forwards if possible (work towards "D" bank)
	for(var i=0;i<3-bank;i++){
		//if prev bank steps in<100, next bank out is moving
		if(arrOut[bank+i]<stepOverlap){arrOut[bank+i+1] = startNext+arrOut[bank+i];}
		//if prev bank steps in>=100 next bank up is fully withdrawn
		else{arrOut[bank+i+1] = bankSteps;}
	}
	return arrOut;
}


function Tavg(relPower){return (560+(30*relPower));} //Function given to find Tavg at specific Rx power levels
function modTempCoeff(boronConcentration){return ((0-33.8)+(boronConcentration*0.01833));} //Function for moderator temperature coefficient (in pcm/F)

// =================================================================================================================================================================
// ===================================================================== GENERIC FUNCTIONS =========================================================================

const problemNo = 2;
let part = "a";

//IMPORT GENERIC FUNCTIONS
const colors = require('colors');
let ci = require("./Exam2functions");

const startNewPart = ci.snp, blank = ci.blk, displayInfo = ci.dI, starLine = ci.sL,solnHeader = ci.sH, colorme = ci.cme, colorOut = ci.cO, spaces = ci.sp();

//import color name variables 
var u=ci.c;
const green=u.g, cyan=u.c, yellow=u.y, magenta=u.m, white=u.w;

ci = null;u = null;//clear data from now unnecessary varibles

// =================================================================================================================================================================
// ===================================================================== PROGRAM RUNTIME ===========================================================================

//Begin Part A
displayInfo("Midterm 2, Problem "+problemNo,"Reece Appling","4/24/2020",part);//----------------------------REPLACE FINISHED DATE-------------------------------------
//The operator increases power from zero to 60% and stops to compare rod positions to their insertion limits.
//	The insertion limit for Bank C is 200 steps withdrawn with a corresponding insertion limit for Bank D of
//	72 steps withdrawn. The actual rod positions at this power are Bank C at 190 steps withdrawn and Bank D
//	at 62 steps withdrawn. What would be the corresponding boron concentration under these conditions?

//Show given Variables
let dPower = [0,0.6];//power change in percent
//declare variables to solve for
let BoA = 0; //this will be the output, boron concentration in ppm
//Calculate actual rod positions given C=190 withdrawn
let rodPositionsIn = banksFromBank(2,228-190);
//calculate same positions but for steps withdrawn
let rodPositionsOut = [0,0,0,0];
for(var i=0;i<4;i++){rodPositionsOut[i]=228-rodPositionsIn[i];}

//Display Rod positions (steps in) for each bank
console.log("Bank          Rod Positions (steps in)");
for(var i=0;i<4;i++){console.log(bankOrder[i],"             ",rodPositionsIn[i]);}blank();

console.log("Bank          Rod Positions (steps withdrawn)");
for(var i=0;i<4;i++){console.log(bankOrder[i],"             ",rodPositionsOut[i]);}blank();

//Calculate step to max ratio for all rod banks (withdrawn form)
let currentBankWorths = [0,0,0,0];
for(var i=0;i<4;i++){currentBankWorths[i]=(rodPositionsIn[i]/228)*bankWorths[i];}
//for(var i=0;i<4;i++){currentBankWorths[i]=(rodPositionsOut[i]/228)*bankWorths[i];}//Uncomment this to switch to the opposite-----------------

//Calculate current worths of bank positions
console.log("Bank           Worths (pcm)");
for(var i=0;i<4;i++){console.log(bankOrder[i],"             ",colorme(currentBankWorths[i].toFixed(2),"yellow"));}blank();

//Calculate BOC static reactivity
console.log("BOC Core Excess reactivity: ",15334,"pcm -> ",15334);
let reactivity = 15334-5600;
console.log("BOC Fixed BP worth: ",-5600,"pcm -> ",reactivity);

//Subtract doppler-only coeff from reactivity
reactivity = reactivity-(9.8*60);
console.log("Doppler-only reactivity worth: ",-9.8*60,"pcm -> ",reactivity);

//subtract rod bank reactivity
let currentRodReactivity = 0;
for(var i=0;i<4;i++){
	currentRodReactivity=currentRodReactivity-currentBankWorths[i];
	reactivity=reactivity-currentBankWorths[i];
	console.log(bankOrder[i]+" Bank worth: ",colorme(currentBankWorths[i].toFixed(2),"yellow"),"pcm -> ",reactivity.toFixed(2));
}

//Calculate dTavg and Tavg
let dTavg = (30*0.6);
let Tavg60 = 560+dTavg;

//Remove only Non-boron portion of MTC from reactivity
reactivity = reactivity-(33.8*dTavg);
console.log("Non-boron portion of MTC: ",-(33.8*dTavg),"pcm -> ",colorme(reactivity.toFixed(2),"yellow"));

//Calculate MTC Boron term and static boron term together
let BoronWorthAtTavg = (0.01833*dTavg)-7;
console.log("Modified Boron Worth: ",BoronWorthAtTavg," pcm/ppm");

//Calculate Boron concentration required, output it
BoA = -reactivity/BoronWorthAtTavg;
console.log("Boron concentration required: ",colorme(BoA.toFixed(4)+" ppm","green"));
blank();
//Print solution for part a
solnHeader(problemNo,part);
console.log(colorme(spaces+BoA.toFixed(4)+" ppm","green"));
blank();


//Begin Part B
part = "b";startNewPart(part);
//What would have been the zero power critical rod position at this boron concentration?

//List variables to find
let bankPosB = [0,0,0,0]; //Vector of steps withdrawn for A,B,C,D, is part of output for this part of the question
let stepsB = 0; //total number of steps withdrawn for this part, part of output
//Copy Boron from last problem
let BoB = BoA;
//Get reactivities
reactivity = 0;
console.log("BOC Core Excess reactivity: ",15334,"pcm -> ",15334);
reactivity = 15334-5600;
console.log("BOC Fixed BP worth: ",-5600,"pcm -> ",reactivity);
console.log("Doppler-only reactivity worth: ",0,"pcm -> ",reactivity);
reactivity = reactivity-(7*BoB);
console.log("Soluble boron reactivity worth: ",colorme((-7*BoB).toFixed(3),"yellow"),"pcm -> ",colorme(reactivity.toFixed(2),"yellow"));
console.log("Control rod reactivity required: ",colorme(reactivity.toFixed(2),"yellow"),"pcm");
//Find a rod position (# of steps away from full in) which makes reactor critical (or barely subcritical)
let dReactivity=0;
var testbank; var test;
//var lowest = [-1,1000];//-----------------------------------------------------------------------------------------------------------0
var leastNeg = [-1,-1000];
//try each of the steps to find best(s)
for(var i=0;i<=(128*4)+100;i++){
	dReactivity = reactivity;
	testbank = bankPositionStepsWithdrawn(i);
	for(var j=0;j<4;j++){dReactivity =dReactivity-((testbank[j]/228)*bankWorths[j]);}
	//if(dReactivity>=0&&dReactivity<lowest[1]){lowest = [i,dReactivity];}//----------------------------------------------------------0
	if(dReactivity<=0&&Math.abs(dReactivity)<Math.abs(leastNeg[1])){leastNeg=[i,dReactivity];}
}
//console.log("Steps withdrawn resulting in smallest positive reactivity:",lowest[0]);//----------------------------------------------0
//console.log("Resulting Reactivity:",lowest[1]);//-----------------------------------------------------------------------------------0
blank();
console.log("Steps withdrawn (overlapping steps count as 1) resulting in smallest (by magnitude) negative reactivity:",leastNeg[0]);
console.log("Resulting Core Reactivity:",colorme(leastNeg[1].toFixed(4),"yellow")," pcm");
blank();
//create solutions object ([0]=positive,[1]=negative final reactivity)
var solnB = [{},{}];
//solnB[0].step = lowest[0];//-----------------------------------------------------------------------------------0
solnB[1].step = leastNeg[0];
//solnB[0].reactivity = lowest[1];//-----------------------------------------------------------------------------------0
solnB[1].reactivity = leastNeg[1];
//Set original solutions variables
stepsB = leastNeg[0];
//bankPosB is currently steps inserted, stepsB is number of steps withdrawn
bankPosB = bankPositionStepsWithdrawn(stepsB);
invertedBankPosB = bankPositionStepsWithdrawn(stepsB);
//alter bankPosB to be steps withdrawn (invert positions)
for(var i=0;i<4;i++){bankPosB[i]=228-bankPosB[i];}
blank();
//Print solutions for part b
solnHeader(problemNo,part);
colorOut([spaces+"Total steps withdrawn (overlap is a single step): ",stepsB],[white,green]);
for(var i=0;i<4;i++){colorOut([spaces+"Bank ",bankOrder[i]," : ",bankPosB[i]," steps withdrawn"],[white,green,white,green,white]);}
blank();console.log(spaces+"Which is Equivalent to...");blank();
colorOut([spaces+"Total steps inserted (overlap is a single step): ",((128*4)+100)-stepsB],[white,green]);
for(var i=0;i<4;i++){colorOut([spaces+"Bank ",bankOrder[i]," : ",invertedBankPosB[i]," steps inserted"],[white,green,white,green,white]);}
blank();



//Begin Part C
part = "c";startNewPart(part);
//To avoid violating insertion limits, the operator borates to bring the C bank to 210 steps withdrawn and the
//	D bank to 82 steps. What is the new boron concentration?

let BoC = 0; //this will be the output, boron concentration in ppm
let CstepsIn = 228-210;
rodPositionsIn = banksFromBank(2,CstepsIn);//Calculate actual rod positions given C=210 withdrawn
for(var i=0;i<4;i++){rodPositionsOut[i]=228-rodPositionsIn[i];}//calculate same positions but for steps withdrawn
//Display Rod positions (steps in) for each bank
console.log("Bank          Rod Positions (steps in)");
for(var i=0;i<4;i++){console.log(bankOrder[i],"             ",rodPositionsIn[i]);}blank();
//Display Rod positions (steps out) for each bank
console.log("Bank          Rod Positions (steps withdrawn)");
for(var i=0;i<4;i++){console.log(bankOrder[i],"             ",rodPositionsOut[i]);}blank();
for(var i=0;i<4;i++){currentBankWorths[i]=(rodPositionsIn[i]/228)*bankWorths[i];}//Calculate step to max ratio for all rod banks (withdrawn form)
//Calculate current worths of bank positions
console.log("Bank           Worths (pcm)");
for(var i=0;i<4;i++){console.log(bankOrder[i],"             ",colorme(currentBankWorths[i].toFixed(2),"yellow"));}blank();
//Calculate dTavg and Tavg
dTavg = (30*0.6);Tavg60 = 560+dTavg;
console.log("Tavg   0% : ",560,"°F");
console.log("Tavg  60% : ",Tavg60,"°F");
console.log("Temperature Change from 0 power : ",dTavg,"°F");blank();
//Get reactivities
//Calculate BOC static reactivity
console.log("BOC Core Excess reactivity: ",15334,"pcm -> ",15334);
reactivity = 15334-5600;//remove Fixed BP Worth
console.log("BOC Fixed BP worth: ",-5600,"pcm -> ",reactivity);
//Subtract doppler reactivity
reactivity = reactivity-(9.8*60)
console.log("Doppler-only reactivity worth: ",-(9.8*60),"pcm -> ",reactivity);
//subtract rod bank reactivities
for(var i=0;i<4;i++){
	reactivity=reactivity-currentBankWorths[i];
	console.log("Bank",bankOrder[i]," current worth: ",colorme(currentBankWorths[i].toFixed(2),"yellow"),"pcm -> ",colorme(reactivity.toFixed(3),"yellow"));
}
//Remove only Non-boron portion of MTC from reactivity
reactivity = reactivity-(33.8*dTavg);
console.log("Non-boron portion of MTC: ",-(33.8*dTavg),"pcm -> ",colorme(reactivity.toFixed(2),"yellow"));
//Calculate MTC Boron term and static boron term together
BoronWorthAtTavg = (0.01833*dTavg)-7;
console.log("Modified Boron Worth: ",BoronWorthAtTavg," pcm/ppm");
//Calculate Boron concentration required, output it
BoC = -reactivity/BoronWorthAtTavg;
console.log("Boron concentration required: ",colorme(BoC.toFixed(4),"green")," ppm");
blank();
//Print solution for part c
solnHeader(problemNo,part);
console.log(colorme(spaces+BoC.toFixed(4)+" ppm","green"));
blank();


//Begin Part D
part = "d";startNewPart(part);
//If the operator were to continue to increase power to 100% at this boron concentration, what would be the final rod position?

let stepsD = 0;
let bankPosD = [0,0,0,0]; //Vector of steps withdrawn for A,B,C,D, is part of output for this part of the question
let BoD = BoC; //Boron ppm from last problem

//Calculate dTavg and Tavg
dTavg = (30*1);Tavg60 = 560+dTavg;
console.log("Tavg   0% : ",560,"°F");
console.log("Tavg 100% : ",Tavg60,"°F");
console.log("Temperature Change from 0 power : ",dTavg,"°F");blank();

//Calculate BOC static reactivity
console.log("BOC Core Excess reactivity: ",15334,"pcm -> ",15334);
reactivity = 15334-5600;//remove Fixed BP Worth
console.log("BOC Fixed BP worth: ",-5600,"pcm -> ",reactivity);
//Subtract doppler reactivity
reactivity = reactivity-(9.8*100)
console.log("Doppler-only reactivity worth: ",-(9.8*100).toFixed(0),"pcm -> ",reactivity);
//Remove MTC from reactivity
reactivity = reactivity-((33.8-(0.01833*BoD))*dTavg);
console.log("MTC worth: ",-((33.8-(0.01833*BoD))*dTavg).toFixed(3),"pcm -> ",colorme(reactivity.toFixed(2),"yellow"));
//Remove soluble boron from reactivity
reactivity = reactivity-(7*BoB);
console.log("Soluble boron reactivity worth: ",colorme((-7*BoD).toFixed(3),"yellow"),"pcm -> ",colorme(reactivity.toFixed(2),"yellow"));
console.log("Control rod reactivity required: ",colorme(reactivity.toFixed(2),"yellow"),"pcm");

//Find a rod position (# of steps away from full in) which makes reactivity barely <=0
leastNeg = [-1,-1000];
for(var i=0;i<=(128*4)+100;i++){
	dReactivity = reactivity;
	testbank = bankPositionStepsWithdrawn(i);
	for(var j=0;j<4;j++){dReactivity =dReactivity-((testbank[j]/228)*bankWorths[j]);}
	if(dReactivity<=0&&Math.abs(dReactivity)<Math.abs(leastNeg[1])){leastNeg=[i,dReactivity];}
}blank();
console.log("Steps withdrawn (overlapping steps count as 1) resulting in smallest (by magnitude) negative reactivity:",leastNeg[0]);
console.log("Resulting Core Reactivity:",colorme(leastNeg[1].toFixed(4),yellow)," pcm");
blank();
//create solutions object ([0]=positive,[1]=negative final reactivity)
var solnD = [{},{}];
//solnB[0].step = lowest[0];//-----------------------------------------------------------------------------------0
solnD[1].step = leastNeg[0];
//solnB[0].reactivity = lowest[1];//-----------------------------------------------------------------------------------0
solnD[1].reactivity = leastNeg[1];
//Set original solutions variables
stepsD = leastNeg[0];
//bankPosB is currently steps inserted, stepsB is number of steps withdrawn
bankPosD = bankPositionStepsWithdrawn(stepsD);
let invertedBankPosD = bankPositionStepsWithdrawn(stepsD);
//alter bankPosB to be steps withdrawn (invert positions)
for(var i=0;i<4;i++){bankPosD[i]=228-bankPosD[i];}
blank();
//Print solutions for part d
solnHeader(problemNo,part);
colorOut([spaces+"Total steps withdrawn (overlap is a single step): ",stepsD],[white,green]);
for(var i=0;i<4;i++){colorOut([spaces+"Bank ",bankOrder[i]," : ",bankPosD[i]," steps withdrawn"],[white,green,white,green,white]);}
blank();console.log(spaces+"Which is Equivalent to...");blank();
colorOut([spaces+"Total steps inserted (overlap is a single step): ",((128*4)+100)-stepsD],[white,green]);
for(var i=0;i<4;i++){colorOut([spaces+"Bank ",bankOrder[i]," : ",invertedBankPosD[i]," steps inserted"],[white,green,white,green,white]);}
blank();



//Begin Part E
part = "e";startNewPart(part);
//It is desired to have the D bank at 216 steps withdrawn at 100 percent power. What would be the necessary boron concentration?

let BoE = 0; //this will be the output, boron concentration in ppm
let DstepsIn = 228-216;
rodPositionsIn = banksFromBank(3,DstepsIn);//Calculate actual rod positions given C=210 withdrawn
for(var i=0;i<4;i++){rodPositionsOut[i]=228-rodPositionsIn[i];}//calculate same positions but for steps withdrawn
//Display Rod positions (steps in) for each bank
console.log("Bank          Rod Positions (steps in)");
for(var i=0;i<4;i++){console.log(bankOrder[i],"             ",rodPositionsIn[i]);}blank();
//Display Rod positions (steps out) for each bank
console.log("Bank          Rod Positions (steps withdrawn)");
for(var i=0;i<4;i++){console.log(bankOrder[i],"             ",rodPositionsOut[i]);}blank();
for(var i=0;i<4;i++){currentBankWorths[i]=(rodPositionsIn[i]/228)*bankWorths[i];}//Calculate step to max ratio for all rod banks (withdrawn form)
//Calculate current worths of bank positions
console.log("Bank           Worths (pcm)");
for(var i=0;i<4;i++){console.log(bankOrder[i],"             ",colorme(currentBankWorths[i].toFixed(2),"yellow"));}blank();
//Calculate dTavg and Tavg
dTavg = (30*1);Tavg100 = 560+dTavg;
console.log("Tavg   0% : ",560,"°F");
console.log("Tavg 100% : ",Tavg100,"°F");
console.log("Temperature Change from 0 power : ",dTavg,"°F");blank();
//Get reactivities
//Calculate BOC static reactivity
console.log("BOC Core Excess reactivity: ",15334,"pcm -> ",15334);
reactivity = 15334-5600;//remove Fixed BP Worth
console.log("BOC Fixed BP worth: ",-5600,"pcm -> ",reactivity);
//Subtract doppler reactivity
reactivity = reactivity-(9.8*100)
console.log("Doppler-only reactivity worth: ",-(9.8*100).toFixed(2),"pcm -> ",reactivity);
//subtract rod bank reactivities
for(var i=0;i<4;i++){
	reactivity=reactivity-currentBankWorths[i];
	console.log("Bank",bankOrder[i]," current worth: ",colorme(currentBankWorths[i].toFixed(2),"yellow"),"pcm -> ",colorme(reactivity.toFixed(3),"yellow"));
}
//Remove only Non-boron portion of MTC from reactivity
reactivity = reactivity-(33.8*dTavg);
console.log("Non-boron portion of MTC: ",-(33.8*dTavg).toFixed(2),"pcm -> ",colorme(reactivity.toFixed(2),"yellow"));
//Calculate MTC Boron term and static boron term together
BoronWorthAtTavg = (0.01833*dTavg)-7;
console.log("Modified Boron Worth: ",BoronWorthAtTavg," pcm/ppm");
//Calculate Boron concentration required, output it
BoE = -reactivity/BoronWorthAtTavg;
console.log("Boron concentration required: ",colorme(BoE.toFixed(4),"green")," ppm");
blank();
//Print solution for part e
solnHeader(problemNo,part);
console.log(colorme(spaces+BoE.toFixed(4)+" ppm","green"));
blank();


//Begin Part F
part = "f";startNewPart(part);
//What would be the boron concentration once Xenon and Samarium have reached equilibrium concentration?

let BoF = 0; //this will be the output, boron concentration in ppm
//Termperature information
console.log("Tavg   0% : ",560,"°F");
console.log("Tavg 100% : ",Tavg100,"°F");
console.log("Temperature Change from 0 power : ",dTavg,"°F");blank();
//Calculate BOC static reactivity
console.log("Core Excess reactivity: ",15334,"pcm -> ",15334);
reactivity = 15334-5600;//remove Fixed BP Worth
console.log("Fixed BP worth: ",-5600,"pcm -> ",reactivity);
//Subtract doppler reactivity
reactivity = reactivity-(9.8*100)
console.log("Doppler-only reactivity worth: ",-(9.8*100).toFixed(0),"pcm -> ",reactivity);
//subtract rod bank reactivities
for(var i=0;i<4;i++){
	reactivity=reactivity-currentBankWorths[i];
	console.log("Bank",bankOrder[i]," current worth: ",colorme(currentBankWorths[i].toFixed(2),"yellow"),"pcm -> ",colorme(reactivity.toFixed(3),"yellow"));
}
//subtract samarium activity
reactivity = reactivity-463
console.log("Samarium reactivity worth: ",-(463).toFixed(0),"pcm -> ",reactivity);
//subtract Xenon activity
reactivity = reactivity-2853
console.log("Xenon reactivity worth: ",-(2853).toFixed(0),"pcm -> ",reactivity);
//Remove only Non-boron portion of MTC from reactivity
reactivity = reactivity-(33.8*dTavg);
console.log("Non-boron portion of MTC: ",-(33.8*dTavg).toFixed(2),"pcm -> ",colorme(reactivity.toFixed(2),"yellow"));

//Calculate MTC Boron term and static boron term together
BoronWorthAtTavg = (0.01833*dTavg)-7;
console.log("Modified Boron Worth: ",BoronWorthAtTavg," pcm/ppm");
//Calculate Boron concentration required, output it
BoF = -reactivity/BoronWorthAtTavg;
console.log("Boron concentration required: ",colorme(BoF.toFixed(4),"green")," ppm");
blank();
//Print solution for part c
solnHeader(problemNo,part);
console.log(colorme(spaces+BoF.toFixed(4)+" ppm","green"));
blank();