//Exam2Q3.js

//Initialize variables for primary and secondary fluids,SG, and SG loss coefficients
let primary = {};let secondary = {};let SG = {};SG.LossCoeffs = {};

secondary.P = 925;//psia
secondary.Ti100 = 460;//°F
secondary.Ti20 = 298;//°F
const SecondaryInletTemp = [secondary.Ti100,secondary.Ti20];

SG.Q = 1284;//MW (heat transfer rate)
SG.n = 15531;//# sg tubes
SG.s = 0.875/12;//ft
SG.Do = 0.625/12;//ft
SG.thk = 0.034/12;//ft
SG.L = 52.3;//ft
SG.k = 9;//Btu/hr-ft-F

SG.LossCoeffs.inout = 1.5;//Inlet and Exit Loss Coefficients
SG.plates = 15; //number of tube support plates
SG.LossCoeffs.plate = 0.5;//Tube support plate loss coefficient

primary.P = 2250;//psia
primary.Th = 604;//°F
primary.Tc = 554;//°F
primary.mTotal = 65.55*(10**6);//lbm/hr for entire SG

//Necessary conversions
let MWtoBtuhr = 3412141.6331279;//Conversion factor from MW to Btu/href
SG.Q = SG.Q*MWtoBtuhr;
const gc = 32.2*(3600**2);//gravity in ft/hr^2




// =================================================================================================================================================================
// ===================================================================== GENERIC FUNCTIONS =========================================================================

//initial problem info
const problemNo = 3;let part = "a";

//import text color library
const colors=require('colors');
//Import generic functions and apply them to constants as necessary
let ci=require("./Exam2functions");
const startNewPart=ci.snp,blank=ci.blk,displayInfo=ci.dI,starLine=ci.sL,solnHeader=ci.sH,colorme=ci.cme,colorOut=ci.cO,spaces=ci.sp(),ct = ci.cO;

//import color name variables 
var u=ci.c;const green=u.g, cyan=u.c, yellow=u.y, magenta=u.m, white=u.w, w=u.w, y=u.y;

//clear data from now unnecessary varibles
ci = null;u = null;

// =================================================================================================================================================================
// ===================================================================== PROGRAM RUNTIME ===========================================================================




//RUNTIME VARIABLES TO CHANGE IN ORDER TO FIND SOLUTIONS!!!!!!!!!!!!!!!!

const partATSteam = 550;//Trial and error (iterative)
const partBTSteam = 603.87;//Trial and error (iterative)
const problemTempTry = [partATSteam,partBTSteam];







//Begin Part A
displayInfo("Midterm 2, Problem "+problemNo,"Reece Appling","4/25/2021",part);
//For the given data, determine the steam flow rate, steam exit temperature and boiling length that correspond to
//	the given tube length. The boiling length can be taken to be that point at with the equilibrium quality is one.
//	You may assume the Thom correlation is valid in the nucleate boiling region.

//write conversion factors and constants
console.log("Conversion factors and constants:");console.log(MWtoBtuhr,"Btu/hr-MW");console.log("gc = ",32.2+" ft/s^2  === ",gc+" ft/hr^2");console.log();
const writeMainFxnChanges = true;


//create function for averaging two numbers
function avg(a,b){return ((a+b)/2);}
//create function for simple outputs with alternating white and yellow info (a=arguments to print) (internally, b=colors array)
function show(...a){let b=[];for(var i=0;i<a.length;i++){((((i+1)%2)>0)?b.push(w):b.push(y));}ct(a,b);}
//create function to set and show (ONE VARIABLE ONLY)
function set(a,b,c){show(a," "+b+" ",c);return b;}
//Set up interpolation function
function interpolate(pt1,pt2,pt3x){let slope=(pt2[1]-pt1[1])/(pt2[0]-pt1[0]);return (pt1[1]+(slope*(pt3x-(pt1[0]))));}

//Set relative power and feed temp as depicted in problem
let relativepower = 1;
let TFeed = SecondaryInletTemp[0];secondary.Tc = SecondaryInletTemp[0];
//Secondary side saturation properties
show("At steam pressure =",secondary.P,"psia, saturation properties for steam:");
let Tsat = set("Tsat:",535.274,"°F");secondary.Tsat = Tsat;
secondary.hf = set("hf:",530.858,"Btu/lbm");
secondary.hg = set("hg:",1195.32,"Btu/lbm");
secondary.Cpf = set("Cpf:",1.26148,"Btu/lbm°F");
secondary.Cpg = set("Cpg:",1.2073,"Btu/lbm°F");
secondary.muf = set("muf:",0.226582,"lbm/ft-hr");
secondary.mug = set("mug:",0.0451934,"lbm/ft-hr");
secondary.kf = set("kf:",0.336141,"Btu/hr-ft°F");
secondary.kg = set("kg:",0.0349346,"Btu/hr-ft°F");
secondary.rhof = set("rhof:",46.8828,"lbm/ft^3");
secondary.rhog = set("rhog:",2.0566,"lbm/ft^3");
secondary.theta = set("theta value:",0.0190872," (freedom units)");
blank();
//display secondary side inlet temp and max outlet temp allowable
console.log("Secondary side inlet temperature: ",secondary.Tc,"°F");
console.log("Secondary side outlet temperature constraints: (535.274(Tsat),604)°F");
//show HL temp
show("Hot  leg Temperature:",primary.Th,"°F");blank();
//insert basic knowledge about a superheated vapor reference point
console.log("The maximum possible Steam outlet temperature and pressure are a reference point for interpolation");
colorOut(["At (P = ",secondary.P," psia) and (T=THot= ",primary.Th," °F), fluid properties are as follows:"],[white,yellow,white,yellow,white]);
secondary.Tu = 604;
secondary.hu = set("h:",1260.98,"Btu/lbm");
secondary.Cpu = set("Cp:",0.808682,"Btu/lbm°F");
secondary.muu = set("mu:",0.0497721,"lbm/ft-hr");
secondary.ku = set("k:",0.0103539,"Btu/hr-ft°F");
secondary.rhou = set("rhog:",1.74776,"lbm/ft^3");blank();
//show/calculate primary loop temperatures
show("Cold leg average Temperature:",primary.Tc,"°F");
primary.Tavg = set("Primary loop average Temperature:",avg(primary.Th,primary.Tc),"°F");blank();
//get hh, hc, havg,hin, Tsat,hf, hg from steam tables
primary.hh = set("Hot  leg enthalpy:",618.877,"Btu/hr");
primary.hc = set("Cold leg enthalpy:",552.188,"Btu/hr");
primary.Cph = set("Hot  Leg Cp:",1.44164,"Btu/lb°F");
primary.Cpc = set("Cold Leg Cp:",1.25246,"Btu/lb°F");
primary.muh = set("Hot  Leg viscosity:",0.196199,"lbm/ft-hr");
primary.muc = set("Cold Leg viscosity:",0.223592,"lbm/ft-hr");
primary.kh = set("Hot  Leg k:",0.30407,"Btu/hr-ft°F");
primary.kc = set("Cold Leg k:",0.335335,"Btu/hr-ft°F");blank();
//Find avg primary properties
console.log("Calculate average primary fluid properties by averaging HL and CL properties");
primary.havg = set("Average primary loop enthalpy:",avg(primary.hh,primary.hc),"Btu/hr");
primary.Cpavg= set("      Average primary loop Cp:",avg(primary.Cph,primary.Cpc),"Btu/lbm°F");
primary.muavg= set("      Average primary loop mu:",avg(primary.muh,primary.muc),"lbm/ft-hr");
primary.kavg = set("      Average primary loop  k:",avg(primary.kh,primary.kc),"Btu/hr-ft°F");blank();
//Find secondary loop properties
show("Secondary loop properties:");
show("from steam tables @ Tfeed and secondary P");
secondary.hc = set("Inlet enthalpy, hin:",441.682,"Btu/lbm");
secondary.Cpc = set("Inlet Cp:",1.12711,"Btu/lbm°F");
secondary.muc = set("Inlet mu:",0.272866,"lbm/ft-hr");
secondary.kc = set("Inlet k:",0.368427,"Btu/hr-ft°F");
secondary.rhoc = set("Inlet rho:",51.1727,"lbm/ft^3");

//Calculate properties at average temperatures (average values of inlet and exit properties for respective areas)
//set values initially to 1x3 0-vectors
secondary.h=[0,0,0];secondary.Cp=[0,0,0];secondary.mu=[0,0,0];secondary.k=[0,0,0];secondary.T=[0,0,0];secondary.rho=[0,0,0];
show("Secondary side calculatable region-average properties");show("(average values of inlet and exit properties for respective region)");
secondary.h[0] = set("Subcooled enthalpy:",avg(secondary.hc,secondary.hf),"Btu/lbm");
secondary.Cp[0] = set("     Subcooled Cp:",avg(secondary.Cpc,secondary.Cpf),"Btu/lbm°F");
secondary.mu[0] = set("     Subcooled mu:",avg(secondary.muc,secondary.muf),"lbm/ft-hr");
secondary.k[0] = set("       Subcooled k:",avg(secondary.kc,secondary.kf),"Btu/hr-ft°F");
secondary.rho[0] = set("   Subcooled rho:",avg(secondary.rhoc,secondary.rhof),"lbm/ft^3");
secondary.h[1] = set("Nucleate Boiling enthalpy:",avg(secondary.hg,secondary.hf),"Btu/lbm");
secondary.Cp[1] = set("     Nucleate Boiling Cp:",avg(secondary.Cpg,secondary.Cpf),"Btu/lbm°F");
secondary.mu[1] = set("     Nucleate Boiling mu:",avg(secondary.mug,secondary.muf),"lbm/ft-hr");
secondary.k[1] = set("       Nucleate Boiling k:",avg(secondary.kg,secondary.kf),"Btu/hr-ft°F");
secondary.rho[1] = set("   Nucleate Boiling rho:",avg(secondary.rhog,secondary.rhof),"lbm/ft^3");
blank();
//Calculate average temperatures for secondary side (except superheated)
//set to blank array
secondary.T[0] = set("Tavg (Subcooled):",(TFeed+Tsat)/2,"°F");
secondary.T[1] = set("Tavg (NB region):",Tsat,"°F (Tsat)");
//Set and echo total heat transfer rate
primary.Q = set("Heat Transfer Rate:",SG.Q*relativepower,"Btu/hr");
	console.log("     Equivalent to: "+(primary.Q/MWtoBtuhr)+" MW");blank();
//Find primary side flow areas
SG.ro = set("Tube outer radius:",SG.Do/2,"ft");
SG.ri = set("Tube inner radius:",SG.ro-SG.thk,"ft");
SG.Di = set("Tube inner Diameter:",SG.ri*2,"ft");
primary.Ax = set("Primary side single-tube cross-sectional Area: ",(Math.PI*(SG.ri**2)),"ft^2");
primary.A  = set("Primary side total cross-sectional Area: ",(primary.Ax*SG.n),"ft^2");
primary.SAx = set("Primary side single-tube surface Area:",Math.PI*SG.Di*SG.L,"ft^2");
primary.SA = set("Primary side total surface area:",primary.SAx*SG.n,"ft^2");
primary.G = set("Primary side mass flux:",primary.mTotal/primary.A,"lbm/hr-ft^2");
primary.m = set("Primary side single-tube mass flow rate:",primary.mTotal/SG.n,"lbm/hr");blank();
//Calculate Re,Pr, then hc (hi)
primary.Re = set("Primary side Reynolds Number:",(primary.G*SG.Di/primary.muavg)," ");
primary.Pr = set("Primary side  Prandlt Number:",(primary.Cpavg*primary.muavg/primary.kavg)," ");
console.log("Because coolant is being cooled in a tube: Nu=0.023*(Re^0.8)*(Pr^0.3)");
console.log("      and hc=k*Nu/De, this results in:");
primary.hi = set("Primary side heat transfer coefficient:",((0.023*Math.pow(primary.Re,0.8)*(primary.Pr,0.3))*primary.kavg/SG.Di),"Btu/hr-°F-ft^2");
blank();
//calculate C for Weisman later
secondary.Ax = set("Shell-side single-channel cross-sectional area:",((Math.sqrt(3)/4)*(SG.s**2))-(Math.PI*(SG.Do**2)/8),"ft^2");
secondary.A = set("Shell-side           total cross-sectional area:",secondary.Ax*SG.n*2,"ft^2");
secondary.Pw = set("Shell-side single-channel wetted perimeter:",Math.PI*SG.Do/2,"ft");
secondary.De = set("Shell-side channel effective Diameter De:",4*secondary.Ax/secondary.Pw,"ft");
secondary.C = set("Shell-side Weisman Constant:",(0.026*(SG.s/SG.Do))-0.006," ");
blank();
//Find thom coefficients
SG.sigma = set("Sigma for thom correlation:",(Math.exp((2*secondary.P)/1260))/(72**2),"");
SG.mThom = set("   m  for thom correlation:",2,"");
//CONTINUE WITH ITERATIVE PROCESSES

//function to give weff nonlinear function only in terms of weff
function weffFxnMaker(hib,rib,rob,kb,Tsatb,Tavgb,steamgen){let hia=hib;let ria=rib;let roa=rob;let ka=kb;let Tsata=Tsatb;let Tavga=Tavgb;let sg=steamgen;
	return (x)=>{let hi=hia;let ri=ria;let ro=roa;let k=ka;let Tavg=Tavga;let sigma=sg.sigma;let mThom=sg.mThom;
		return (((ro*sigma*(10**6))*((x-Tsat)**SG.mThom))-((((1/(hi*ri))+(Math.log(ro/ri)/k))**(-1))*(Tavg-x)));
	};
}

//function for log mean temperature difference
function LMTD(a,b,c,d){let e=a-c;let f=b-d;return ((e-f)/Math.log(e/f));}


function testTemp(Tvar,primin,secin,sgin,doPrint,isPt1){
	//rename arguments
	let prim = primin;
	let sec = secin;
	let sg = sgin;
	let isDev = doPrint;
	//set up output object, put Steam outlet temp in it
	let fxOut = {};fxOut.Th = Tvar;
	//OUTPUT OBECT WILL CONTAIN:
	//Th,m,L[],Lboil,Ltot,dL,dPElev,dPAcc,dPLocal,dPFric,dPFricForm
	
	//generic functions for selective data output within this function
	let sett = (isDev) ? set : ((a,b,c) => {return b;});let showw = (isDev) ? show : ((args) => {});let skip = (isDev) ? blank : (()=>{});
	
	
	
	//assign Steam outlet temp to necessary variables//Tavg
	sec.Th = sett("secondary outlet temp:",Tvar,"°F");
	//interpolate steam exit enthalpy, display it followed by hg
	sec.hh = sett("secondary outlet enthalpy:",interpolate([sec.Tsat,sec.hg],[sec.Tu,sec.hu],sec.Th),"Btu/lbm");
	showw(        "     Steam sat vapor h=hg=",sec.hg," Btu/lbm");
	//calculate mass flow rate, put it in output variable
	fxOut.m = sett("Corresponding mass flow:",prim.Q/(sec.hh-sec.hc),"lbm/hr");
	let m = sett("Corresponding mass flow:",prim.Q/(sec.hh-sec.hc),"lbm/hr");
	
	//Calculate steam exit properties
	showw("Interpolating secondary exit properties (from saturated vapor and reference superheated vapor)");
	sec.Cph = sett("Secondary exit Cp:",interpolate([sec.hu,sec.Cpu],[sec.hg,sec.Cpg],sec.hh),"");
	sec.muh = sett("Secondary exit mu:",interpolate([sec.hu,sec.muu],[sec.hg,sec.mug],sec.hh),"");
	sec.kh  = sett("Secondary exit k:",interpolate([sec.hu,sec.ku],[sec.hg,sec.kg],sec.hh),"");
	sec.rhoh  = sett("Secondary exit rho:",interpolate([sec.hu,sec.rhou],[sec.hg,sec.rhog],sec.hh),"");
	
	//Find superheat region fluid properties, temp,h,Cp,mu,k by averaging them across the region (if not superheated, recalc required region)
	sec.h[2] = sett("Avg superheated h:",avg(sec.hh,sec.hg)," ");
	sec.T[2] = sett("Avg superheated T:",avg(sec.Th,sec.Tsat)," ");
	sec.Cp[2] = sett("Avg superheated Cp:",avg(sec.Cph,sec.Cpg)," ");
	sec.mu[2] = sett("Avg superheated mu:",avg(sec.muh,sec.mug)," ");
	sec.k[2] = sett("Avg superheated k:",avg(sec.kh,sec.kg)," ");
	sec.rho[2] = sett("Avg superheated rho:",avg(sec.rhoh,sec.rhog)," ");skip();
	//calculate shell side mass flux
	sec.G = sett("Shell-side mass flux:",fxOut.m/secondary.A,"lbm/hr-ft^2");
	//calculate discrete Q values
	let Qsf = sett("Qsf:",fxOut.m*(sec.hf-sec.hc),"Btu/hr");
	let Qnb = sett("Qnb:",fxOut.m*(sec.hg-sec.hf),"Btu/hr");
	//let Qsh = sett("Qsh:",fxOut.m*(sec.hh-sec.hg),"Btu/hr");
	let Qsh = sett("Qsh:",prim.Q-Qsf-Qnb,"Btu/hr");
	//Calculate points 1 and 2 enthalpies
	prim.h1 = sett("h1:",prim.hh-(Qsf/prim.mTotal),"Btu/lbm");
	prim.h2 = sett("h2:",prim.hc+(Qsh/prim.mTotal),"Btu/lbm");
	//Calculate fluid temps at points 1 and 2 (as well as their average)
	prim.T1 = sett("T1:",prim.Th-((prim.hh-prim.h1)/prim.Cpavg),"deg F");
	prim.T2 = sett("T2:",prim.Tc+((prim.h2-prim.hc)/prim.Cpavg),"deg F");
	prim.T12 = sett("Tbar (region 2):",avg(prim.T1,prim.T2),"deg F");
	//Calculate points 1 and 2 fluid properties based on HL and CL fluid properties and h1/2
	prim.Cp1 = sett("Cp1:",interpolate([prim.hh,prim.Cph],[prim.hc,prim.Cpc],prim.h1),"Btu/lbm°F");
	prim.Cp2 = sett("Cp2:",interpolate([prim.hh,prim.Cph],[prim.hc,prim.Cpc],prim.h2),"Btu/lbm°F");
	prim.mu1 = sett("mu1:",interpolate([prim.hh,prim.muh],[prim.hc,prim.muc],prim.h1),"lbm/ft-hr");
	prim.mu2 = sett("mu2:",interpolate([prim.hh,prim.muh],[prim.hc,prim.muc],prim.h2),"lbm/ft-hr");
	prim.k1 = sett("k1:",interpolate([prim.hh,prim.kh],[prim.hc,prim.kc],prim.h1),"Btu/hr-ft°F");
	prim.k2 = sett("k2:",interpolate([prim.hh,prim.kh],[prim.hc,prim.kc],prim.h2),"Btu/hr-ft°F");
	//Set a couple of 1x3 arrays of 0 before more calculations
	let dT = [0,0,0];let ho=[0,0,0];fxOut.L = [0,0,0];
	//Calculate all log mean temperature differences
	dT[0] = sett("LMTD (subcooled):",LMTD(prim.T2,prim.Tc,sec.Tsat,sec.Tc)," "); //LMTD for subcooled
	dT[1] = sett("LMTD (NB):",LMTD(prim.T1,prim.T2,sec.Tsat,sec.Tsat)," "); //LMTD for nb
	dT[2] = sett("LMTD (superheat):",LMTD(prim.Th,prim.T1,sec.Th,sec.Tsat)," "); //LMTD for Superheated
	//calculate heat transfer coeffs
	ho[0] = sett("Secondary heat transfer coefficient (subcooled):",((sec.k[0]/sec.De)*(sec.C*((sec.G*sec.De/sec.mu[0])**0.8)*((sec.Cp[0]*sec.mu[0]/sec.k[0])**(1/3)))),"Btu/hr-°F-ft^2");
	ho[2] = sett("Secondary heat transfer coefficient (superheat):",((sec.k[2]/sec.De)*(sec.C*((sec.G*sec.De/sec.mu[2])**0.8)*((sec.Cp[2]*sec.mu[2]/sec.k[2])**(1/3)))),"Btu/hr-°F-ft^2");
	//calculate region length
	fxOut.L[0] = sett("Subcooled Length:",((Qsf/dT[0])*((1/(prim.hi*sg.ri))+(Math.log(sg.ro/sg.ri)/sg.k)+(1/(ho[0]*sg.ro))))/(2*Math.PI*sg.n)," ft");
	fxOut.L[2] = sett("Superheat Length:",((Qsh/dT[2])*((1/(prim.hi*sg.ri))+(Math.log(sg.ro/sg.ri)/sg.k)+(1/(ho[2]*sg.ro))))/(2*Math.PI*sg.n)," ft");
	//fxOut.L[1] = sg.L-fxOut.L[2]-fxOut.L[0];
	//iteratively solve for Weff
	let wfx=weffFxnMaker(prim.hi,sg.ri,sg.ro,sg.k,sec.Tsat,prim.T12,sg);//make weff(x) equation
	let bounds=[prim.T1,sec.Tsat];//set bounds to check within
	let nIntervals=10000;//set number of intervals
	let dX = (bounds[0]-bounds[1])/nIntervals;//find interval size
	//set initial values for sign (true=positive),fin=iterative solution found, weff=found Weff value;
	let sign = false;let fin = false;
	let weff = 0;let evald = 0;
	if(wfx(bounds[1])>0){sign=true;}//If initial value is positive, set sign to positive
	//go through intervals, break if sign changes
	for (var i=1;i<nIntervals;i++){
		let xTest=(bounds[1]+(i*dX));//find x and y values for position i
		evald=wfx(xTest);
		if((sign==true&&evald<=0)||(sign==false&&evald>=0)){fin=true;weff=xTest;break;}//if sign changes, break from loop
	}
	if(!fin){console.log(colorme("WARNING","red"));console.log("Iteration could not complete");}//Warn  if iteration failure
	showw("Effective wall temp:",weff," deg F");//display weff result if enabled
	//solve for NB ho
	ho[1]= sett("NB hx coeff:",(10**6)*sg.sigma*((weff-sec.Tsat))," ");
	//solve for NB region length with NB ho
	fxOut.L[1] = sett("Nucleate Boiling Length:",((Qnb/dT[1])*((1/(prim.hi*sg.ri))+(Math.log(sg.ro/sg.ri)/sg.k)+(1/(ho[1]*sg.ro))))/(2*Math.PI*sg.n)," ft");
	//Assign boiling length, calculated total length, variation, and output variation if required
	fxOut.Lboil = fxOut.L[0]+fxOut.L[1];
	fxOut.Ltot = fxOut.L[0]+fxOut.L[1]+fxOut.L[2];
	fxOut.dL = sett("Boiling length variation:",fxOut.L[0]+fxOut.L[1]+fxOut.L[2]-sg.L,"ft");
	
	
	
	//STARTING PARTS C/D
	
	
	
	//function for equilibrium quality in NB region given H
	let xe = (z)=>{let start=fxOut.L[0];let finish=fxOut.Lboil;return ((z-start)/(finish-start));};
	let martinelli = (x,muf,mug,rhof,rhog) => {return Math.sqrt(((muf/mug)**0.2)*(((1-x)/x)**1.8)*(rhog/rhof));};
	let frictionMultiplierSquared = (x,muf,mug,rhof,rhog) => {
		let mX = martinelli(x,muf,mug,rhof,rhog);
		return (((1)+(20/mX)+(1/(mX**2)))*((1-x)**1.8));
	};
	let integral = (start,fin,func) => {
		let dx = (fin-start)/1000;
		let iSum = 0;
		for(var i=0;i<1000;i++){iSum=iSum+(dx*(func(start+(dx/2)+(dx*i))));}
		return iSum;
	};
	let fricMultH = (z)=>{
		let muf=sec.muf;let mug=sec.mug; let rhof = sec.rhof; let rhog=sec.rhog;
		return frictionMultiplierSquared(xe(z),muf,mug,rhof,rhog);
	};
	let avgFricMult = ((1/fxOut.L[0])*integral(fxOut.L[0],fxOut.L[1],fricMultH));
	let Vgj = sett("Drift Velocity:",1.41*(((sec.theta*gc*(sec.rhof-sec.rhog))/(sec.rhof**2))**(1/4))," ft/sec")
	let voidqual = (x) => {return 1/((1.13*(1+((sec.rhog/sec.rhof)*((1-x)/x))))+((sec.rhog*Vgj)/(sec.G*x)));};
	let avgVoid = integral(fxOut.L[0],fxOut.Lboil,voidqual)/fxOut.L[1];
	
	
	
	
	
	//Find heat flux (linear and surfact) of each area
	let Qarr=[Qsf,Qnb,Qsh];
	let linearFlux = [0,1,2].map(x => (Qarr[x]/(Math.PI*sg.Do)));
	let heatFluxAvg = [0,1,2].map(x => (linearFlux[x]/(Math.PI*sg.Do)));
	for(var i=0;i<3;i++){
		showw("Average linear heat flux for region ",i+": "+linearFlux[i]," Btu/hr-ft");
		showw("Average        heat flux for region ",i+": "+heatFluxAvg[i]," Btu/hr-ft^2");
	}
	//find plate locations for local losses
	let dPlate = (sg.plates-1)/fxOut.Ltot;let plates = [];
	for(var i=0;i<sg.plates;i++){plates.push(i*dPlate);}
	//set up pressure drop variables
	let O4=[0,0,0,0];
	fxOut.dPElev=O4;fxOut.dPAcc=O4;fxOut.dPLocal=O4;fxOut.dPFric=O4;fxOut.dPFricForm=O4;
	//SINGLE PHASE
	//Elevation pressure drops
	fxOut.dPElev[0] = sett("Subcooled Elevation dP:",sec.rho[0]*fxOut.L[0]/144,"psia");
	fxOut.dPElev[1] = sett("Nucleate boiling Elevation dP:",fxOut.L[1]*((avgVoid*sec.rhog)+((1-avgVoid)*(sec.rhof))),"psia");
	fxOut.dPElev[2] = sett("Superheat Elevation dP:",sec.rho[2]*fxOut.L[2]/144,"psia");
	//Acceleration pressure drops
	let accelSinglePhase = (G,gcc,rhoin,rhoout) =>{return (((G**2)/gcc)*((1/rhoout)-(1/rhoin)));};//(G^2/gc)((1/rho2)-(1/rho1))
	fxOut.dPAcc[0] = sett("Subcooled Acceleration dP:",accelSinglePhase(sec.G,gc,sec.rhoc,sec.rhof)/144,"psia");
	fxOut.dPAcc[1]=sett("Nucleate Boiling Acceleration dP:",accelSinglePhase(sec.G,gc,sec.rhof,sec.rhog)/144,"psia");
	fxOut.dPAcc[2] = sett("Superheat Acceleration dP:",accelSinglePhase(sec.G,gc,sec.rhog,sec.rhoh)/1444,"psia");
	//local pressure losses
	//1phase sum of (Kj G^2)/(2 rhoj gc)
	let localSinglePhase = (Kj,G,rho,gcc) => {return ((Kj*(G**2))/(2*rho*gcc));};
	//Do inlet and outlet
	fxOut.dPLocal[0]=(localSinglePhase(sg.LossCoeffs.inout.plate,sec.G,sec.rhoc,gc)/144);//inlet loss
	fxOut.dPLocal[1]=0;
	fxOut.dPLocal[2]=(localSinglePhase(sg.LossCoeffs.inout.plate,sec.G,sec.rhoh,gc)/144);//outlet loss
	//Plate losses
	for(var i=0;i<plates.length;i++){
		if(plates[i]<=fxOut.L[0]){fxOut.dPLocal[0]=fxOut.dPLocal[0]+(localSinglePhase(sg.LossCoeffs.plate,sec.G,sec.rho[0],gc)/144);}//In single phase subcooled region
		else{if(plates[i]>=fxOut.Lboil){fxOut.dPLocal[2]=fxOut.dPLocal[2]+(localSinglePhase(sg.LossCoeffs.plate,sec.G,sec.rho[2],gc)/144);
		}else{fxOut.dPLocal[2]=fxOut.dPLocal[2]+(localSinglePhase(sg.LossCoeffs.plate,sec.G,sec.rho[2],gc)*frictionMultiplierSquared(xe(plates[i]),sec.muf,sec.mug,sec.rhof,sec.rhog)/144);}//twophase
		}//In single phase superheat region
			
	}
	//calculate relative roughness and reynolds numbers
	showw("Assuming smooth drawn tubing...");
	let roughness = 0.000005;
	showw("Relative roughness =",roughness/sec.De," ");
	showw("Reynolds Number (Subcooled):",(sec.G*sec.De/sec.mu[0])," "+sec.mu[0]);
	showw("Reynolds Number  sat liquid:",(sec.G*sec.De/sec.muf)," "+sec.muf);
	showw("Reynolds Number (Superheat):",(sec.G*sec.De/sec.mu[2])," "+sec.mu[2]);
	//get friction factors
	let fcoeff=[0,0];
	if(isPt1){fcoeff=[0.0225,0.022,0.017];}//part c values found from Moody Chart
	else{fcoeff=[(64/(sec.G*sec.De/sec.mu[0])),(64/(sec.G*sec.De/sec.muf)),0.028];}//part d values found from Moody Chart
	showw("Checking these values against the Moody chart...");
	showw("Average friction factors = (Subcooled):",fcoeff[0],", (Superheat):",fcoeff[2]," ");
	showw("Average friction factors = (saturated liquid):",fcoeff[1]," ");
	//calculate friction losses
	let fricSinglePhase = (f,H,G,De,rhoavg,gcc) =>{return ((f*H*(G**2))/(2*De*rhoavg*gcc));};//(f H G^2)/(De 2 rhoavg gc)
	fxOut.dPFric[0]=sett("Friction: ",(fricSinglePhase(fcoeff[0],fxOut.L[0],sec.G,sec.De,sec.rho[0],gc)/144)," ");
	fxOut.dPFric[1]=sett("Friction: ",(fricSinglePhase(fcoeff[1],fxOut.Lboil*avgFricMult,sec.G,sec.De,sec.rho[0],gc)/144)," ");
	fxOut.dPFric[2]=sett("Friction: ",(fricSinglePhase(fcoeff[2],fxOut.L[2],sec.G,sec.De,sec.rho[2],gc)/144)," ");
	
	fxOut.dPFric[3]=sett("Friction: ",fxOut.dPFric[0]+fxOut.dPFric[1]+fxOut.dPFric[2]," ");
	fxOut.dPLocal[3]=sett("Local: ",fxOut.dPLocal[0]+fxOut.dPLocal[1]+fxOut.dPLocal[2]," ");
	fxOut.dPAcc[3]=sett("Acceleration: ",fxOut.dPAcc[0]+fxOut.dPAcc[1]+fxOut.dPAcc[2]," ");
	fxOut.dPElev[3]=sett("Elevation: ",fxOut.dPElev[0]+fxOut.dPElev[1]+fxOut.dPElev[2]," ");
	
	fxOut.dP = fxOut.dPFric[3]+fxOut.dPLocal[3]+fxOut.dPAcc[3]+fxOut.dPElev[3];
	console.log(fxOut.dP);
	fxOut.ratio = fxOut.dPElev[3]/fxOut.dP;
	
	return fxOut;
}
let resA = testTemp(problemTempTry[0],primary,secondary,SG,writeMainFxnChanges,true);blank();//get results
//Print solution for part a
solnHeader(problemNo,part);
let mA = resA[0];//save steam flow rate for later
console.log(spaces+"Steam flow rate: "+resA.m+" lbm/hr");
console.log(spaces+"Steam exit temperature: "+resA.Th+"°F");
console.log(spaces+"Boiling length: "+resA.Lboil+" ft");
blank();
























//Begin Part B
part = "b";startNewPart(part);
//Assuming a constant Tave, repeat your analysis at 20% power. Assuming the reference feed demand is correct at
//	100% power, what would be the shim component of the reference feed demand at 20% power?
//Output variables
var shimComponent = 0; //
//Set relative power as depicted in problem
relativepower = 0.2;
//Set feedtemp as depicted in problem
TFeed = SecondaryInletTemp[1];
secondary.Tc = SecondaryInletTemp[1];
console.log("Secondary side inlet temperature: ",secondary.Tc,"°F");
console.log("Secondary side outlet temperature constraints: (535.274(Tsat),604)°F");
//show/calculate primary loop temperatures
show("Hot  leg Temperature:",primary.Th,"°F");
show("Cold leg average Temperature:",primary.Tc,"°F");
primary.Tavg = set("Primary loop average Temperature:",avg(primary.Th,primary.Tc),"°F");blank();
//Find secondary loop properties
show("Secondary loop properties:");
show("from steam tables @ Tfeed and secondary P");
secondary.hc = set("Inlet enthalpy, hin:",441.682,"Btu/lbm");
secondary.Cpc = set("Inlet Cp:",1.12711,"Btu/lbm°F");
secondary.muc = set("Inlet mu:",0.272866,"lbm/ft-hr");
secondary.kc = set("Inlet k:",0.368427,"Btu/hr-ft°F");
secondary.rhoc = set("Inlet rho:",57.5817,"lbm/ft^3");
//Calculate properties at average temperatures (average values of inlet and exit properties for respective areas)

show("Secondary side calculatable region-average properties");show("(average values of inlet and exit properties for respective region)");
secondary.h[0] = set("Subcooled enthalpy:",avg(secondary.hc,secondary.hf),"Btu/lbm");
secondary.Cp[0] = set("     Subcooled Cp:",avg(secondary.Cpc,secondary.Cpf),"Btu/lbm°F");
secondary.mu[0] = set("     Subcooled mu:",avg(secondary.muc,secondary.muf),"lbm/ft-hr");
secondary.k[0] = set("       Subcooled k:",avg(secondary.kc,secondary.kf),"Btu/hr-ft°F");
secondary.rho[0] = set("   Subcooled rho:",avg(secondary.rhoc,secondary.rhof),"lbm/ft^3");
blank();
//Calculate average temperatures for secondary side (except superheated)
secondary.T[0] = set("Tavg (Subcooled):",(secondary.Tc+Tsat)/2,"°F");
secondary.T[1] = set("Tavg (NB region):",Tsat,"°F (Tsat)");
//Set and echo total heat transfer rate
primary.Q = set("Heat Transfer Rate:",SG.Q*relativepower,"Btu/hr");
console.log("     Equivalent to: "+(primary.Q/MWtoBtuhr)+" MW");blank();
//CONTINUE WITH ITERATIVE PROCESSES
let resB = testTemp(problemTempTry[1],primary,secondary,SG,writeMainFxnChanges,false);blank();
//Print solution for part b
solnHeader(problemNo,part);
console.log(spaces+"Steam flow rate: "+resB.m+" lbm/hr");
console.log(spaces+"Steam exit temperature: "+resB.Th+"°F");
console.log(spaces+"Boiling length: "+resB.Lboil+" ft");
blank();



//Begin Part C
part = "c";startNewPart(part);
//For nominal operating conditions, determine the total pressure drop and its individual components assuming an
//	Equilibrium Model is valid in the two-phase region. What fraction of the total pressure drop is the elevation
//	component? For simplicity, you can assume a uniform (but not equal) heat flux in each of the three heat
//	transfer regions. You may also assume constant, average fluid properties in the single phase regions. Assuming
//	saturated liquid, what would be the equivalent “level” corresponding to the total pressure drop?

blank();
//Print solution for part c
solnHeader(problemNo,part);
colorOut([spaces+"Total Pressure Drop              : ",resA.dP," psia"],[white,green,yellow]);
colorOut([spaces+"Elevation Pressure Drop          : ",resA.dPElev[3]," psia"],[white,green,yellow]);
colorOut([spaces+"Friction and Forms Pressure Drop : ",resA.dPFric[3]+resA.dPLocal[3]," psia"],[white,green,yellow]);
colorOut([spaces+"Acceleration Pressure Drop       : ",resA.dPAcc[3]," psia"],[white,green,yellow]);
blank();
colorOut([spaces+"Elevation pressure drop/Total pressure drop : ",resA.ratio],[white,green]);
blank();


//Begin Part D
part = "d";startNewPart(part);
//Compare your results in part c to those at the 20% power condition.
blank();
//Print solution for part d
solnHeader(problemNo,part);
colorOut([spaces+"Total Pressure Drop              : ",resB.dP," psia"],[white,green,yellow]);
colorOut([spaces+"Elevation Pressure Drop          : ",resB.dPElev[3]," psia"],[white,green,yellow]);
colorOut([spaces+"Friction and Forms Pressure Drop : ",resB.dPFric[3]+resB.dPLocal[3]," psia"],[white,green,yellow]);
colorOut([spaces+"Acceleration Pressure Drop       : ",resB.dPAcc[3]," psia"],[white,green,yellow]);
blank();
colorOut([spaces+"Elevation pressure drop/Total pressure drop : ",resB.ratio],[white,green]);
blank();



